const express = require("express");
require("dotenv").config();
const cors = require("cors");
const app = express();
const port = process.env.PORT || 5000;
const { MongoClient, ServerApiVersion, ObjectId } = require("mongodb");

// middle ware
app.use(cors());
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
// ADD TO MONGO DB
const uri =
  "mongodb+srv://shemanto:2GktPVekIL7SiQQf@cluster0.0p3wt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
});

async function run() {
  await client.connect();
  const wareHouseCollection = client
    .db("warehouse-furniture")
    .collection("furniture");
  const newCollection = client.db("warehouse-furniture").collection("newitem");

  // GET ALL DATA GET API
  app.get("/products", async (req, res) => {
    const query = {};
    const cursor = wareHouseCollection.find(query);
    const result = await cursor.toArray();
    res.send(result);
  });
  //  GET DATA SINGLE GET API
  app.get("/product/:id", async (req, res) => {
    const id = req.params.id;
    const query = { _id: ObjectId(id) };
    const cursor = await wareHouseCollection.findOne(query);
    res.send(cursor);
  });
  // DELETE API
  app.delete("/product/:id", async (req, res) => {
    const id = req.params.id;
    const query = { _id: ObjectId(id) };
    const result = await wareHouseCollection.deleteOne(query);
    res.send(result);
  });

  app.post("/newItem", async (req, res) => {
    const newAdd = req.body;
    console.log(newAdd);
    const result = await newCollection.insertOne(newAdd);
    res.send(result);
  });

  // GET METHOD FILTER BY EMAIL
  app.get("/newItem", async (req, res) => {
    const email = req.query.email;
    console.log(email);
    const query = { email: email };
    const cursor = newCollection.find(query);
    const result = await cursor.toArray();
    res.send(result);
  });
  // DELETE API
  app.delete("/newItem/:id", async (req, res) => {
    const id = req.params.id;
    const query = { _id: ObjectId(id) };
    const result = await newCollection.deleteOne(query);
    res.send(result);
  });

  // UPDATE METHOD
  app.put("/product/:id", async (req, res) => {
    const id = req.params._id;
    const query = { _id: ObjectId(id) };
    const options = { upsert: true };
    const updateInfo = req.body;
    console.log(updateInfo);
    console.log("here is " + updateInfo);
    const updateDoc = {
      $set: {
        ...updateInfo,
      },
    };
    const result = await wareHouseCollection.updateOne(
      query,
      updateDoc,
      options
    );
    res.send(result);
  });
  // update product
  // app.put("/product/:id", async (req, res) => {
  //   const id = req.params.id;
  //   const query = { _id: ObjectId(id) };
  //   const updateInfo = req.body;
  //   console.log(updateInfo);
  //   const options = { upsert: true };
  //   const updateDoc = {
  //     $set: {
  //       ...updateInfo,
  //     },
  //   };
  //   const result = await wareHouseCollection.updateOne(
  //     query,
  //     updateDoc,
  //     options
  //   );
  //   res.send(result);
  // });
}
run().catch(console.dir);

// client.connect((err) => {

//   const collection = client.db("warehouse-furniture").collection("furniture");
//   // perform actions on the collection object
//   client.close();
// });

// client.connect((err) => {
//   const collection = client.db("warehouse-furniture").collection("furniture");
//   console.log("server connection");
//   client.close();
// });

app.get("/", (req, res) => {
  res.send("hello world");
});

app.listen(port, () => {
  console.log("working on server");
});
// shemanto
// 2GktPVekIL7SiQQf
